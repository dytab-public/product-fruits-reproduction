import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FruityComponent } from '@productfruit-test/fruity';

@Component({
  standalone: true,
  imports: [RouterModule, FruityComponent],
  selector: 'productfruit-test-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'productfruit-test';
}
