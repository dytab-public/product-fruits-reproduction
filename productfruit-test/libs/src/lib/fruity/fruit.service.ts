import { Injectable } from '@angular/core';
import { productFruits } from 'product-fruits';

@Injectable({
  providedIn: 'root',
})
export class FruitService {
  constructor() {}

  initProductFruits() {
    const productFruitUser: ProductFruitsUserObject = {
      username: 'foo',
    };
    productFruits.init('KNBiOjUoJnltRARp', 'de', productFruitUser, {
      disableLocationChangeDetection: true,
    });
  }
}
