import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FruitService } from './fruit.service';

@Component({
  selector: 'productfruit-test-fruity',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './fruity.component.html',
  styleUrl: './fruity.component.css',
})
export class FruityComponent implements OnInit {
  constructor(private fruitService: FruitService) {}

  ngOnInit(): void {
    this.fruitService.initProductFruits();
  }
}
